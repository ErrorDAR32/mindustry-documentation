# Welcome!

**Welcome to the Official Unofficial documentation for [Mindustry](https://github.com/Anuken/Mindustry)!** This is still very limited, and *very* under construction. You can view the progress on the [Wiki Planner](https://docs.google.com/spreadsheets/d/1wIRdj09dGPf28hX6y-YM_uP2Xp78T11raeDPbm21Ee8/edit?usp=sharing). 

This wiki aims to fully cover the unstable and unfinished 4.0 builds, just as the old (and still unfinished, sadly) [3.5 Docs](https://mindustry.wikia.com/wiki/Mindustry_Wiki) also aimed to do. (You are welcome to contribute to that wiki, too!)

**Contribution is highly encouraged.** If you find anything in the game that is not listed in these articles and would like to help, please feel free to hop onto the [GitLab Repository](https://gitlab.com/MindustryModders/mindustry-documentation) and open a Merge Request with the changes. If you are new to Markdown, I **strongly** advise you to visit this [beautiful guide](https://commonmark.org/help/) by CommonMark. It has a tutorial (Click "Try our 10-minute Markdown Tutorial") to help you learn Markdown, and a quick reference for the most important aspects of Markdown.

**To get started with Mindustry, head on over to [here](GettingStarted.md)!**

**Important!** Most public values (in technical section) are protected as the data is taken from the MML repo.
