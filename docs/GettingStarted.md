# Getting Started

Getting started with Mindustry is easy. This article covers how to install Mindustry on different platforms and situations and gives a brief introduction of the game.

## Introduction

Mindustry is a tower-defense real time strategy game in which you are in an alien planet trying to survive against waves of enemies that increment in difficulty constantly, either online in a server or offline with the campaign. You can play against other players too on a server or over LAN.

### Basics

When you start the game, you will encounter the main menu, in wich you can click **play** to acces the zones campaing, **Join game** to acces games over LAN, **Custom Game** to create an offline game with custom or predefined rules, **Editor** to acces the map editor **maps** to manage already existing maps, **about** to know a litle bit more about the game, **settings** wich is self-explanatory, and **Quit** for quitting the game.
<img src="../img/mainm.PNG" width=20% height=20%>
_Main Menu screenshot showing all the listed buttons above._

### Gameplay

_A tutorial can be found [here](https://mindustrymodders.gitlab.io/mindustry-documentation/Beginner%20Tutorials/Basics/) this just cover the very basics of the experience whe you first play Mindustry_

If you click play on the main menu you will access the zones screen, in which there should be only one map unlocked called _ground zero_ and 50 copper on the top left corner, the back and tech tree button, you should click _ground zero_ to start your first game.

when you start your game you can pause with the space bar, to calmly analyze your screen: on the top-left is a wave counter and timer, indicating in which wave you are, and how many time you have left until the next one and a play-like button to start the next wave inmediatly, resetting the wave timer. on your top-right lies a handful minimap, and finally on your bottom-left is your blocks menu. where you can select which blocks you want to build. 
You should start by picking up resources yourself and gain enough to automate the process of getting those resources, so you can expand your base, build defences, and gain/craft more advances resources. Finally the objective of zones campaign are:

1. Pick and launch enough resources to unlock the next zone.
2. Reach the designated wave to unlock the next zone.
3. Use the resources you launched to unlock more things in the tech tree.

#### Tech tree

the tech tree is a submenu of zones in where you can spend your g

The game is tile-based, so your buildings will always be aligned to the grid, 

## Typical Setup

This is your typical, run-of-the-mill setup process.

### Desktop

1. Visit the game's [itch.io page](https://anuke.itch.io/mindustry), then download a copy of the game from there.
2. Once the `.zip` file is downloaded, unzip it. Usually that is just done by opening it like normal. When it's done, navigate to the folder it extracted into.
3. 
    1. **Windows**: simply open `desktop-release.exe`.
    2. **MacOS**: run `Mindustry.app.`
    3. **Linux**: run `Mindustry`. 

If you have JRE already installed (which is recommended), you can also run `desktop-release.jar` on Windows and Linux.

### Android

This particular section covers how to install Mindustry's **latest builds** on Android. You might be here because you don't know what sort of Mindustry everyone's talking about on the Discord, or heard of `build xx`. If so, please read on.

1. Open Google Play Store and visit Mindustry's page.
2. Scroll down a bit and notice the "Become a beta tester" section. Press "Join now" and confirm. Fully signing up will take a while, so be patient.
3. The green button will then say "Update". Update and enjoy!

### iOS

The latest released builds are available on Apple TestFlight.

## For Contributors

If you would like to contribute to the game's source code, the best way to go is to compile the game yourself or use the builds provided [here](https://jenkins.hellomouse.net/job/mindustry/). To compile it yourself:

1. If you haven't already, install at least JRE and JDK 8. 
2. Open a terminal in the root directory, then run these commands: 
    1. For Windows:
        1. Running: `gradlew desktop:run`
        2. Building: `gradlew desktop:dist`
    2. For Linux:
        1. Running: `./gradlew desktop:run`
        2. Building: `./gradlew desktop:dist`
    3. For Server builds, replace `desktop` with `server`. Example: `./gradlew server:run`

## For aspiring server hosts

Please keep in mind that doing this does not make you automatically cool. **Use the local LAN feature if you only want to play with one or two friends.**

A dedicated Linux or Windows machine is **highly** recommended for this.

1. If you haven't already, install at least JRE and JDK 8.
2. Download the desired server release from [itch.io](itch.io), or compile one yourself. 
3. Open a terminal or TTY session then change `cd` to the directory the JAR is placed in.
4. Run `java -jar server-release.jar`. The commands are explained in the `help` command.

If you have come this far, you should already have the skill and time to be able to port-forward if you need to.

If you do not know how to port-forward, Google "portforward your-router instructions", "your-router" being your router's model. **Portforwarding will require you to have access to your router's admin settings, which will usually be password-protected.** If you do not have access, use LogMeIn Hamachi. To find your IP, Google "what is my IP".

### NEVER SHARE YOUR PUBLIC IP WITH THE PUBLIC, UNLESS YOU ACKNOWLEDGE THE IMPLICATIONS OF DOING SO!

Your public IP is tied to your household, and if it falls into the wrong hands, it can be used to **easily** find your approximate location, even down to the city or block you live in. There is more that can happen; such as DoS attacks, data and information exploitation and collection, and unwanted access to other open ports in your network. **Exercise caution, do your research, and use a VPN if possible.**

If you would like to host a public server for public use, it is recommended that you use a domain name or DNS service to mask your IP a little bit, or use a cloud service e.g. Amazon AWS which is much safer.